#include <iostream>
#include "graphics.hpp"

double tx, ty;
vector<vector<double>> T = {
    {1, 0, tx},
    {0, 1, ty},
    {0, 0, 1}};

vector<vector<double>> V = {
    {0, 0}, {0, 0}, {1, 1}};

vector<vector<double>> res = {
    {0, 0}, {0, 0}, {1, 1}};

void translate() {
  MatrixMultiply(T, V, res, 3, 3, 2);
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 2; ++j)
      cout << res[i][j] << ' ';
    cout << '\n';
  }
  for (int i = 0; i < 3; ++i)
    for (int j = 0; j < 2; ++j)
      V[i][j] = res[i][j];
}

void display() {
  cout << "Enter line end points(4)\n";
  cin >> V[0][0] >> V[1][0] >> V[0][1] >> V[1][1];
  drawLine(V[0][0], V[1][0], V[0][1], V[1][1], white);
  glFlush();
  cout << "Enter translation(2)\n";
  cin >> tx >> ty;
  T[0][2] = tx;
  T[1][2] = ty;
  translate();
  glClear(GL_COLOR_BUFFER_BIT);
  drawLine(V[0][0], V[1][0], V[0][1], V[1][1], white);
  glutSwapBuffers();
}

int main(int argc, char *argv[]) {
  init(&argc, argv);
}
