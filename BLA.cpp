#include <iostream>
#include "graphics.hpp"

void BLA(float x1, float y1, float x2, float y2) {
  float dx = x2 - x1, dy = y2 - y1;
  float steps = abs(x2 - x1);
  float P = 2 * abs(dy) - abs(dx);
  float x, y;
  if (x2 > x1) {
    x = x1;
    y = y1;
  } else {
    x = x2;
    y = y2;
  }
  for (int k = 0; k <= steps; ++k) {
    float prevy = y;
    putpixel(x, y, 0, white);
    x = x + 1;
    if (P > 0)
      y = y + 1;
    P = P + 2 * dy - 2 * dx * (y - prevy);
  }
}

void display() {
  BLA(100, 100, 600, 500);
  glutSwapBuffers();
}

int main(int argc, char *argv[]) {
  init(&argc, argv);
}
