#include <iostream>
#include "graphics.hpp"

void ellipse(double xc, double yc, float a, float b) {
  float x = 0, y = b;
  float p = b * b - a * a * b + a * a / 4;
  while (b * b * x <= a * a * y) {
    if (p < 0) {
      ++x;
      p = p + 2 * b * b * x + b * b;
    } else {
      ++x;
      --y;
      p = p + 2 * b * b * x - 2 * a * a * y - b * b;
    }
    putpixel(xc + x, yc + y, 0, red);
    putpixel(xc + x, yc - y, 0, red);
    putpixel(xc - x, yc + y, 0, green);
    putpixel(xc - x, yc - y, 0, green);
  }

  p = b * b * (x + 0.5) * (x + 0.5) + a * a * (y - 1) * (y - 1) - a * a * b * b;

  while (y > 0) {
    if (p <= 0) {
      x++;
      y--;
      p = p + 2 * b * b * x - 2 * a * a * y + a * a;
    }

    else {
      y--;
      p = p - 2 * a * a * y + a * a;
    }

    putpixel(xc + x, yc + y, 0, blue);
    putpixel(xc + x, yc - y, 0, blue);
    putpixel(xc - x, yc + y, 0, white);
    putpixel(xc - x, yc - y, 0, white);
  }
}

void display() {
  ellipse(512, 384, 400, 200);
  glFlush();  // Render now
}

int main(int argc, char *argv[]) {
  init(&argc, argv);
}
