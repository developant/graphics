#include <cmath>
#include <iostream>
#include "graphics.hpp"

double theta;
vector<vector<double>> R = {
    {cos(theta), -sin(theta), 0},
    {sin(theta), cos(theta), 0},
    {0, 0, 1}};

vector<vector<double>> V = {
    {0, 0}, {0, 0}, {1, 1}};

vector<vector<double>> res = {
    {0, 0}, {0, 0}, {1, 1}};

void rotate() {
  MatrixMultiply(R, V, res, 3, 3, 2);
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 2; ++j)
      cout << res[i][j] << ' ';
    cout << '\n';
  }
  for (int i = 0; i < 3; ++i)
    for (int j = 0; j < 2; ++j)
      V[i][j] = res[i][j];
}

void display() {
  cout << "Enter line end points(4)\n";
  cin >> V[0][0] >> V[1][0] >> V[0][1] >> V[1][1];
  drawLine(V[0][0], V[1][0], V[0][1], V[1][1], white);
  glFlush();
  cout << "Enter rotation(rad)\n";
  cin >> theta;
  R[0][0] = cos(theta);
  R[0][1] = -sin(theta);
  R[1][0] = sin(theta);
  R[1][1] = cos(theta);
  rotate();
  glClear(GL_COLOR_BUFFER_BIT);
  drawLine(V[0][0], V[1][0], V[0][1], V[1][1], white);
  glFlush();
}

int main(int argc, char *argv[]) {
  init(&argc, argv);
}
