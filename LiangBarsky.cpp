#include <iostream>
#include "graphics.hpp"

double xmax, ymax, xmin, ymin;
double x1, x2, y1, y2;
double P[4], q[4];

void assign() {
  P[0] = x1 - x2;
  P[1] = x2 - x1;
  P[2] = y1 - y2;
  P[3] = y2 - y1;
  q[0] = x1 - xmin;
  q[1] = xmax - x1;
  q[2] = y1 - ymin;
  q[3] = ymax - y1;
}

bool check() {
  for (int k = 0; k < 4; ++k) {
    if (P[k] == 0 && q[k] < 0)
      return 0;
  }
  return 1;
}

bool Clip() {
  double u1 = 0, u2 = 1, r;
  for (int k = 0; k < 4; ++k) {
    r = q[k] / P[k];
    if (P[k] < 0)
      u1 = max(u1, r);
    else
      u2 = min(u2, r);
  }
  if (u1 >= u2)
    return 0;
  x2 = x1 + P[1] * u2;
  y2 = y1 + P[3] * u2;
  x1 = x1 + P[1] * u1;
  y1 = y1 + P[3] * u1;
  return 1;
}

void display() {
  cout << "Enter extreme points of the clipping window, rectangle in order (800*800)\n";
  cin >> xmin >> ymin >> xmax >> ymax;
  drawRectangle(xmin, ymin, xmax, ymax, white);
  glutSwapBuffers();
  cout << "Enter end points of the line\n";
  cin >> x1 >> y1 >> x2 >> y2;
  drawLine(x1, y1, x2, y2, red);
  glutSwapBuffers();
  char c = '\0';
  while (c != 'Y' && c != 'y') {
    cout << "Clip ?(Y/N)\n";
    cin >> c;
  }
  glClear(GL_COLOR_BUFFER_BIT);
  assign();
  if (check() && Clip()) {
    drawRectangle(xmin, ymin, xmax, ymax, white);
    drawLine(x1, y1, x2, y2, blue);
  } else
    drawRectangle(xmin, ymin, xmax, ymax, white);
  glutSwapBuffers();
}

int main(int argc, char *argv[]) {
  init(&argc, argv);
}
