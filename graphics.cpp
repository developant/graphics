#include "graphics.hpp"
#include <iostream>

float red[3] = {1.0f, 0.0f, 0.0f};
float green[3] = {0.0f, 1.0f, 0.0f};
float blue[3] = {0.0f, 0.0f, 1.0f};
float white[3] = {1.0f, 1.0f, 1.0f};

int roundoff(double x) {
  if (x < 0.0)
    return (int)(x - 0.5);
  else
    return (int)(x + 0.5);
}

void init(int *argc, char **argv) {
  glutInit(argc, argv);                  // Initialize GLUT
  glutInitWindowSize(800, 800);          // Set the window's initial width & height
  glutInitWindowPosition(0, 0);          // Position the window's initial top-left corner
  glutCreateWindow("Graphics");          // Create a window with the given title
  gluOrtho2D(0, 800, 0, 800);            //  specifies the projection matrix
  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);  // Set background color to black and opaque
  glClear(GL_COLOR_BUFFER_BIT);          // Clear the color buffer (background)
  glutDisplayFunc(display);              // Register display callback handler for window re-paint
  glutMainLoop();                        // Enter the event-processing loop
}

void putpixel(float x, float y, float z, float a[3]) {
  glPointSize(2);
  glBegin(GL_POINTS);  // HERE THE POINTS SHOULD BE CREATED
  glColor3f(a[0], a[1], a[2]);
  glVertex3f(x, y, z);  //  Specify points in 3d plane
  std::cout << x << ' ' << y << ' ' << z << '\n';
  glEnd();
}

void drawLine(double x1, double y1, double x2, double y2, float a[3]) {
  glLineWidth(2.5);
  glColor3f(a[0], a[1], a[2]);
  glBegin(GL_LINES);
  glVertex2d(x1, y1);
  glVertex2d(x2, y2);
  glEnd();
}

void drawRectangle(double x1, double y1, double x2, double y2, float a[3]) {
  glColor3f(a[0], a[1], a[2]);
  glRectd(x1, y1, x2, y2);
}

void drawPolygon(vector<pair<double, double>> v, int n, float a[3]) {
  glColor3f(a[0], a[1], a[2]);
  glBegin(GL_POLYGON);
  for (int i = 0; i < n; ++i)
    glVertex2d(v[i].first, v[i].second);
  glEnd();
}

void MatrixMultiply(vector<vector<double>> &mat1, vector<vector<double>> &mat2, vector<vector<double>> &res,
                    int n, int m, int r) {
  int x, i, j;
  for (i = 0; i < n; i++) {
    for (j = 0; j < r; j++) {
      res[i][j] = 0;
      for (x = 0; x < m; x++) {
        res[i][j] += mat1[i][x] * mat2[x][j];
      }
    }
  }
}