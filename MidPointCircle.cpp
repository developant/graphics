#include <iostream>
#include "graphics.hpp"

void draw_points(unsigned int x, unsigned int y, unsigned int xc, unsigned int yc) {
  putpixel(xc - x, yc + y, 0, white);
  putpixel(xc + x, yc - y, 0, white);
  putpixel(xc + x, yc + y, 0, white);
  putpixel(xc - x, yc - y, 0, white);
  putpixel(xc + y, yc + x, 0, white);
  putpixel(xc - y, yc + x, 0, white);
  putpixel(xc + y, yc - x, 0, white);
  putpixel(xc - y, yc - x, 0, white);
}

void mid_circle(double xc, double yc, double r) {
  unsigned int x = 0, y = r;
  int p = 1 - r;
  draw_points(x, y, xc, yc);
  while (x < y) {
    draw_points(x, y, xc, yc);
    if (p < 0) {
      x = x + 1;
      p = p + 2 * x + 1;
    } else {
      x = x + 1;
      y = y - 1;
      p += 2 * x - 2 * y + 1;
    }
  }
}

void display() {
  mid_circle(512, 384, 200);
  glFlush();  // Render now
  // glutSwapBuffers(); //  reqd only when shaders are being used
}

int main(int argc, char **argv) {
  init(&argc, argv);
}