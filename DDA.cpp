#include <iostream>
#include "graphics.hpp"

void DDA(float x1, float y1, float x2, float y2) {
  float dx = x2 - x1, dy = y2 - y1;
  float steps = 0;
  if (abs(dx) > abs(dy))
    steps = abs(dx);
  else
    steps = abs(dy);
  float x_inc = dx / steps, y_inc = dy / steps;
  float x = x1, y = y1;
  putpixel(roundoff(x), roundoff(y), 0, white);
  for (int k = 1; k <= steps; ++k) {
    x = x + x_inc;
    y = y + y_inc;
    putpixel(roundoff(x), roundoff(y), 0, white);
  }
}

void display() {
  DDA(100, 100, 500, 500);
  glFlush();  // Render now
}

int main(int argc, char *argv[]) {
  init(&argc, argv);
}
