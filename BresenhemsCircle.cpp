#include<iostream>
#include"graphics.hpp"
#include<unistd.h>

void draw_points(unsigned int x, unsigned int y, unsigned int xc, unsigned int yc){
  usleep(10000);
  putpixel(xc-x, yc+y, 0, white);
  putpixel(xc+x, yc-y, 0, white);
  putpixel(xc+x, yc+y, 0, white);
  putpixel(xc-x, yc-y, 0, white);
  putpixel(xc+y, yc+x, 0, white);
  putpixel(xc-y, yc+x, 0, white);
  putpixel(xc+y, yc-x, 0, white);
  putpixel(xc-y, yc-x, 0, white);
}

void BCDA(int xc, int yc, int r){
  int x=0, y=r;
  int P = 3-2*r;
  int prevy=y;
  draw_points(x, y, xc, yc);
  while(x<y){
    prevy = y;
    draw_points(x, y, xc, yc);
    if(P<0){
      P = P+4*x+6;
      x=x+1;
    }
    else{
      P = P+4*(x-y)+10;
      x=x+1; y=y-1;  
    }
  }
}

void display(){
  glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // Set background color to black and opaque
  glClear(GL_COLOR_BUFFER_BIT);         // Clear the color buffer (background)
  BCDA(400, 400, 200);
}

int main(int argc, char *argv[]){
  init(&argc, argv);
}
