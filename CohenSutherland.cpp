#include <bitset>
#include <iostream>
#include "graphics.hpp"

#define B bitset<4>

B TOP("1000");
B BOTTOM("0100");
B RIGHT("0010");
B LEFT("0001");

double xmax, ymax, xmin, ymin;

B ComputeCode(double x, double y) {
  B b;
  if (y > ymax)
    b[3] = 1;
  if (y < ymin)
    b[2] = 1;
  if (x > xmax)
    b[1] = 1;
  if (x < xmin)
    b[0] = 1;
  return b;
}

int check(B b1, B b2) {
  if ((b1 | b2) == 0)
    return 1;
  if ((b1 & b2) != 0)
    return 0;
  return -1;
}

void Clip(double &x, double &y, double m, B b) {
  if ((b & TOP) != 0) {
    x = x + (1 / m) * (ymax - y);
    y = ymax;
  } else if ((b & RIGHT) != 0) {
    y = y + m * (xmax - x);
    x = xmax;
  } else if ((b & BOTTOM) != 0) {
    y = y + m * (xmin - x);
    x = xmin;
  } else if ((b & LEFT) != 0) {
    x = x + (1 / m) * (ymin - y);
    y = ymin;
  }
}

void display() {
  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);  // Set background color to black and opaque
  glClear(GL_COLOR_BUFFER_BIT);          // Clear the color buffer (background)

  cout << "Enter the end points of the clipping plane\n"
          "xmin ymin xmax ymax\n";
  cin >> xmin >> ymin >> xmax >> ymax;
  drawRectangle(xmin, ymin, xmax, ymax, white);
  glutSwapBuffers();
  cout << "Enter end points of the line\n"
          "xleft yleft xright yright\n";
  double x1, y1, x2, y2;
  cin >> x1 >> y1 >> x2 >> y2;
  drawLine(x1, y1, x2, y2, red);
  glutSwapBuffers();
  char c = '\0';
  while (c != 'Y' && c != 'y') {
    cout << "Clip ?(Y/N)\n";
    cin >> c;
  }
  B b1 = ComputeCode(x1, y1), b2 = ComputeCode(x2, y2);
  int chk = check(b1, b2);
  if (chk == -1) {
    double m = (y2 - y1) / (x2 - x1);
    Clip(x1, y1, m, b1);
    Clip(x2, y2, m, b2);
    b1 = ComputeCode(x1, y1);
    b2 = ComputeCode(x2, y2);
    int chk = check(b1, b2);
  }
  if (!chk) {
    glClearColor(0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT);
    drawRectangle(xmin, ymin, xmax, ymax, white);
  }
  if (chk) {
    glClearColor(0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT);
    drawRectangle(xmin, ymin, xmax, ymax, white);
    drawLine(x1, y1, x2, y2, blue);
  }
  glutSwapBuffers();
}

int main(int argc, char *argv[]) {
  init(&argc, argv);
  return 0;
}
