#include <cmath>
#include <iostream>
#include "graphics.hpp"

#define P pair<double, double>

vector<P> V, N;
P C;

double dot(P A, P B) {
  return (A.first * B.first + A.second * B.second);
}

P operator-(const P &A, const P &B) {
  return make_pair(A.first - B.first, A.second - B.second);
}

P operator+(const P &A, const P &B) {
  return make_pair(A.first + B.first, A.second + B.second);
}

P operator*(const P &A, const double t) {
  return make_pair(t * A.first, t * A.second);
}

void computeNormal(int n = V.size()) {
  double m;
  P prev = V[0];
  double x = -1, y;
  for (int i = 1; i < n; ++i) {
    m = -(V[i].first - prev.first) / (V[i].second - prev.second);
    P d = V[i] - prev;
    int lr = 1, tb = 1;
    if (V[i - 1].first > C.first)
      lr = -1;
    if (V[i - 1].second > C.second)
      tb = -1;
    N[i - 1].first = lr * abs(cos(atan(m)));
    N[i - 1].second = tb * abs(sin(atan(m)));
    prev = V[i];
  }
  m = -(V[0].first - prev.first) / (V[0].second - prev.second);
  int lr = 1, tb = 1;
  if (V[n - 1].first > C.first)
    lr = -1;
  if (V[n - 1].second > C.second)
    tb = -1;
  N[n - 1].first = lr * abs(cos(atan(m)));
  N[n - 1].second = tb * abs(sin(atan(m)));
}

int check(P A, P B, int n) {
  P p = B - V[0];
  double d1 = dot(p, N[0]);
  P q = A - V[0];
  double d2 = dot(q, N[0]);
  if (d1 < 0 && d2 < 0)
    return 0;
  if (d1 >= 0 && d2 >= 0)
    return 1;
  return -1;
}

void Clip(P A, P B, double &tE, double &tL, int n = V.size()) {
  tE = 0;
  tL = 1;
  for (int i = 0; i < n; ++i) {
    double d = dot((B - A), N[i]);
    if (d == 0)
      continue;
    double t = dot((A - V[i]), N[i]) / dot((A - B), N[i]);
    if (d > 0 && t > tE)
      tE = t;
    else if (d < 0 && t < tL)
      tL = t;
  }
}

void display() {
  cout << "Enter the number of vertices\n";
  int n;
  cin >> n;
  V.resize(n);
  N.resize(n);
  cout << "Enter the coordinates of the vertices in order\n";
  for (int i = 0; i < n; ++i) {
    cin >> V[i].first >> V[i].second;
    C = C + V[i];
  }
  C.first /= n;
  C.second /= n;
  drawPolygon(V, n, white);
  glutSwapBuffers();
  cout << "Enter the co-ordinates of the line(4)\n";
  P A, B;
  cin >> A.first >> A.second >> B.first >> B.second;
  drawLine(A.first, A.second, B.first, B.second, red);
  glutSwapBuffers();
  char c = '\0';
  while (c != 'Y' && c != 'y') {
    cout << "Clip? (Y/N)\n";
    cin >> c;
  }
  computeNormal();
  int chk = check(A, B, n);
  glClearColor(0, 0, 0, 1);
  glClear(GL_COLOR_BUFFER_BIT);
  drawPolygon(V, n, white);
  if (chk == -1) {
    double tE, tL;
    Clip(A, B, tE, tL);
    if (tE > tL)
      chk = 0;
    else {
      P E = A + (B - A) * tE;
      P L = A + (B - A) * tL;
      drawLine(E.first, E.second, L.first, L.second, blue);
    }
  }
  if (chk == 1)
    drawLine(A.first, A.second, B.first, B.second, blue);
  glutSwapBuffers();
}

int main(int argc, char *argv[]) {
  init(&argc, argv);
}