#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <GL/glut.h>
#include <vector>
using namespace std;

extern float red[3];
extern float green[3];
extern float blue[3];
extern float white[3];

int roundoff(double x);
void init(int *argc, char **argv);
void putpixel(float x, float y, float z, float a[3]);
void drawLine(double x1, double y1, double x2, double y2, float a[3]);
void drawRectangle(double x1, double y1, double x2, double y2, float a[3]);
void drawPolygon(vector<pair<double, double>> v, int n, float a[3]);
void MatrixMultiply(vector<vector<double>> &mat1, vector<vector<double>> &mat2, vector<vector<double>> &res,
                    int n, int m, int r);
void display();

#endif
